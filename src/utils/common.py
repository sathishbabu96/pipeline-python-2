"""
exec.py: Description of what exec.py does.
"""

__author__ = "S Sathish Babu"
__date__ = "30/03/20 Monday 3:45 PM"
__email__ = "sathish.babu@zohocorp.com"

import inspect
from time import time

from bytecode import Bytecode, Instr


class exec_stats:

    def __init__(self):
        self.stage = 1
        self.overall_stats = {
            'start_time': time(),
            'end_time': time(),
            'total_time': 0,
            'output': None
        }

    def __call__(self, func):
        def _wrapper(*args, **kwargs):
            start = time()
            name = args[1].__name__
            result = func(*args, **kwargs)
            end = time()
            self.overall_stats[f'stage-{self.stage}'] = {
                'function': name,
                'params': {
                    'args': args[2:],
                    'kwargs': kwargs
                },
                'start_time': start,
                'end_time': end,
                'time_taken': round(end - start, 3),
                'output': result
            }
            self.stage += 1
            self.overall_stats['end_time'] = time()
            self.overall_stats['total_time'] = round(self.overall_stats['end_time'] - self.overall_stats['start_time'],
                                                     2)
            self.overall_stats['output'] = result
            return result

        return _wrapper


class dynamic:

    def __init__(self):
        pass

    def __call__(self, func):

        def _wrapper(*args, **kwargs):
            p_object = args[0]
            name = args[1].__name__
            # dynamic params that need to be set
            dynamic_params = p_object.dynamic_params.get(name, None)
            # get full args spec of the function
            args_spec = inspect.getfullargspec(args[1])
            args_spec_args = args_spec.args

            # print('argspec:', args_spec)
            # print('dynamic parmas:', dynamic_params)
            # print('*args before:', args)

            # If dynamic value for a parameter is present, then add it to
            # custom kwargs as it must the be value on which the function
            # must be applied. Remove those parameters from *args and
            # append custom kwargs to kwargs.
            custom_kwargs = {}
            if len(args) > 2:
                func_args = args[2] if isinstance(args[2], tuple) else tuple([args[2]])
                # print('func args:', func_args)
                for index, value in enumerate(func_args):
                    if index < len(args_spec_args):
                        custom_kwargs[args_spec_args[index]] = value
                # print('custom kwargs after dynamic:', custom_kwargs)
            # Unpack function returned args
            # _args = []
            # if len(args) > 0:
            #     for arg in args:
            #         if isinstance(arg, tuple) and len(arg) > 0:
            #             for _arg in arg:
            #                 _args.append(_arg)
            #         else:
            #             _args.append(arg)
            #     print('_args before:', _args)
            #     print('returned args:', _args[2:])
            # args_ = _args[:2]
            # if len(_args[2:]) == len(args_spec.args):
            #     # check for dynamic params and remove it from args
            #     # update kwargs with dynamic params
            #     for index, arg in enumerate(args_spec.args):
            #         if arg in dynamic_params:
            #             kwargs[arg] = dynamic_params[arg]
            #         else:
            #             args_.append(_args[index])
            # else:
            #     if args_spec.defaults:
            #         args_spec_args = args_spec_args[:-len(args_spec.defaults)]
            # print('args spec args:', args_spec_args)
            # __args = args_
            #
            # _to_be_removed = []
            # for index, arg in enumerate(args_spec_args):
            #     if arg in dynamic_params:
            #         kwargs[arg] = dynamic_params[arg]
            #         _to_be_removed.append(index + 2)
            # If arg is in dynamic params remove it from args
            # also,
            # if args_spec.defaults:
            #     __args = args_spec.args[:len(args_spec.defaults)]
            #     if len(_args[2:]) == len(args_spec.args):
            #         _args = _args[:-len(args_spec.defaults)]
            # else:
            #     __args = args_spec.args
            # print('_args after default:', _args)
            # print('argspec args:', __args)
            # for index, arg in enumerate(__args):
            #     if arg in dynamic_params and len(_args) > 2:
            #         _to_be_removed.append(index + 2)
            #         kwargs[arg] = dynamic_params[arg]
            # print('to be removed:', _to_be_removed)
            # for index in _to_be_removed[::-1]:
            #     if index < len(_args):
            #         _args.pop(index)
            # _args = tuple(_args[:2]) + tuple(_args[2:])
            # args = _args
            # print('_args after:', _args)
            # print('*args after:', args)
            # print('**kwargs:', kwargs)
            # args = args[:2]
            # print('final:', args)
            args = args[:2]
            if dynamic_params:
                kwargs = {**kwargs, **custom_kwargs, **dynamic_params}
            else:
                kwargs = {**kwargs, **custom_kwargs}
            # print('##' * 50)
            return func(*args, **kwargs)

        return _wrapper


class track:

    def __init__(self, varnames):
        self.varnames = varnames
        self.metrics = {}

    def __call__(self, func):
        print('logging variables {} of function {}'.format(
            ', '.join(self.varnames), func.__name__
        ))
        code = Bytecode.from_code(func.__code__)
        extra_code = [Instr('STORE_FAST', '_res')] + \
                     [Instr('LOAD_FAST', name) for name in self.varnames] + \
                     [Instr('BUILD_TUPLE', len(self.varnames)), 
                      Instr('STORE_FAST', '_debug_tuple'), 
                      Instr('LOAD_FAST', '_res'), 
                      Instr('LOAD_FAST', '_debug_tuple'), 
                      Instr('BUILD_TUPLE', 2), 
                      Instr('STORE_FAST', '_result_tuple'), 
                      Instr('LOAD_FAST', '_result_tuple')]
        code[-1:-1] = extra_code
        func.__code__ = code.to_code()

        def wrapper(*args, **kwargs):
            res, values = func(*args, **kwargs)
            name = func.__name__
            for index, varname in enumerate(self.varnames):
                if name not in self.metrics:
                    self.metrics[name] = {}
                if varname in self.metrics:
                    self.metrics[name][varname].append(values[index])
                else:
                    self.metrics[name][varname] = [values[index]]

            return res

        return wrapper
