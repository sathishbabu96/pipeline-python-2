"""
test2.py: Description of what test.py does.
"""

__author__ = "S Sathish Babu"
__date__ = "30/03/20 Monday 7:34 PM"
__email__ = "sathish.babu@zohocorp.com"

from collections import Counter

from src.pipeline import Pipeline
from src.utils.common import track


def parse_html(file_path):
    with open(file_path) as fp:
        lines = fp.readlines()
    words = [word for line in lines for word in line.split(' ')]
    return words, 2


def word_count(words, test1, test2=2):
    print(test1, test2)
    counter = Counter(words)
    return counter.most_common(5)


pipeline = Pipeline(parse_html, word_count)
pipeline.dynamic_params = {
    'parse_html': {
        'file_path': 'sample2.txt'
    },
    'word_count': {
        # 'words': ['I', ' am', 'red'],
        # 'test1': 10,
        # 'test2': 1000
    }
}
pipeline.start(file_path='sample1.txt')
