"""
pipeline.py: Description of what pipeline.py does.
"""

__author__ = "S Sathish Babu"
__date__ = "30/03/20 Monday 2:46 PM"
__email__ = "sathish.babu@zohocorp.com"

import inspect
from pprint import pprint
from typing import Callable, List, Any, Dict, AnyStr

from src.utils.common import (
    exec_stats as _exec_stats,
    dynamic as _dynamic
)

exec_stats = _exec_stats()
dynamic = _dynamic()


class Pipeline:

    __stages: List[Callable]
    __overall_result: Dict[AnyStr, Any]

    def __init__(self, *stages: Callable):
        self.__stages = []
        self.__overall_result = {}
        self.__dynamic_params = {}
        for stage in stages:
            if isinstance(stage, Callable):
                self.__stages.append(stage)
            else:
                raise TypeError(f"Given stages does contain a callalbe. {stage} is not a callable.")

    def start(self, *args, **kwargs):
        first_stage = self.__stages.pop(0)

        _input = self.execute(first_stage, *args, **kwargs)
        for stage in self.__stages:
            _output = self.execute(stage, _input)
            _input = _output

        print('     Overall result     '.center(100, '#'))
        pprint(self.__overall_result)
        print('     Overall stats     '.center(100, '#'))
        pprint(exec_stats.overall_stats)

    @exec_stats
    @dynamic
    def execute(self, stage, *args, **kwargs) -> Any:

        name = stage.__name__
        # args_spec = inspect.getfullargspec(stage)
        # args_spec_args = args_spec.args
        #
        # # print('argspec:', args_spec)
        # # print('dynamic parmas:', self.__dynamic_params[name])
        # print('before:', args, kwargs)
        # if name != 'wrapper':
        #     custom_kwargs = {}
        #     if len(args) > 0:
        #         func_args = args[0] if isinstance(args[0], tuple) else tuple([args[0]])
        #         # print('func args:', func_args)
        #         for index, value in enumerate(func_args):
        #             if index < len(args_spec_args):
        #                 custom_kwargs[args_spec_args[index]] = value
        #         # print('custom kwargs after dynamic:', custom_kwargs)
        #
        #     if self.__dynamic_params[name]:
        #         kwargs = {**kwargs, **custom_kwargs, **self.__dynamic_params[name]}
        #     else:
        #         kwargs = {**kwargs, **custom_kwargs}
        # if len(args) > 0:
        #     args = args[0]
        # print('after:', args, kwargs)
        # print('##' * 40)
        self.__overall_result[name] = {
            'args': args,
            'kwargs': kwargs
        }
        result = stage(*args, **kwargs)
        self.__overall_result[name]['ouput'] = result
        return result

    @property
    def dynamic_params(self):
        return self.__dynamic_params

    @dynamic_params.setter
    def dynamic_params(self, value):
        self.__dynamic_params = value

    def set_dynamic_params(self, function, key, value):
        self.__dynamic_params[function] = {key: value}
